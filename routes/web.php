<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware(['auth'])->group(function () {
    //
    Route::post('/login','App\Http\Controllers\LoginController@login')->name('login');

    Route::get('/dashboard', function () {
        return view('dashboard/dashboard');
        
    })->name('dashboardView');
    
    Route::get('/productos/index', function () {
        return view('productos/create');
        
    })->name('productos.create');
    
    
    
    Route::get('/productos/create', function () {
        return view('productos/create');
        
    })->name('productos.create');
    
    Route::post('/guardarProducto','App\Http\Controllers\ProductosController@guardar')->name('login');
    
    
    
    Route::get('/productos/VerCompra','App\Http\Controllers\ProductosController@index')->name('productos.VerCompra');
    
    Route::get('/comprarProducto/{producto}','App\Http\Controllers\ProductosController@comprar')->name('productos.comprar');
    
    Route::get('/facturas/pendientes','App\Http\Controllers\FacturaController@index')->name('facturas.pendientes');
    
    Route::get('/facturas/facturar/{id_user}','App\Http\Controllers\FacturaController@facturar')->name('facturas.facturar');
    
    Route::get('/facturas/facturadas','App\Http\Controllers\FacturaController@facturadas')->name('facturas.facturadas');
    
    Route::get('/facturas/detalle/{idfactura}','App\Http\Controllers\FacturaController@detalleFactura')->name('facturas.detalle');
    
    
    
    
    
    
    
    

});




Route::get('/', function () {
    return view('sessionViews/login');
})->name('loginView');


Route::get('/registrarse', function () {
    return view('sessionViews/registrarse');
});



