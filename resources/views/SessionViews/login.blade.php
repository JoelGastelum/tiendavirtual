<div class="row">
    <div class="card">
        <div class="content">
            <form action="/login" method="POST">
                @csrf
                <h2 class="title"> Iniciar Sesión </h2>
                <div class="form-input">
                    <label for="email">Email:</label>
                    <input type="text" class="form-control" id="email" name="email">
                </div>
                <div class="form-input">
                    <label for="password">Password:</label>
                    <input type="password" class="form-control" id="password" name="password">
                </div>
                @if(isset($error))
                <p>{{ $error }}</p>
            @endif
                <div class="form-input">
                    <button class="btn btn-login"> Iniciar Sesión</button>
                    <a href="/registrarse" class="btn-link"><button class="btn btn-registrarse" type="button"> Quiero crear una cuenta </button></a>
                </div>
            </form>
          
            
        
        </div>
    </div>

</div>


<style>
 
    .row{
        display: flex;
        justify-content:center;
        align-items:center;
    }
    .card{
        margin-top:8rem;
        border:   0.3px #444444;
        box-shadow: 0 0 10px #444444;
        min-width:30rem;
    }
    
    .title {
        font-size:3em;
        text-align:center;
    }
    
    .content{
        display:flex;
        padding:1em;
        flex-direction:column;
        
    }
    
    .form-control{
        display:flex;
        border-radius:1em;
        width:100%;
        height:3em;
        margin:.4em;
    }
    
    .form-input{
        margin:2rem;
        align-self:center;
        text-align:center;
    }
    
    .btn {
        width:100%;
        border-radius:8px;
        margin:0.3rem;
        height:2rem;
    }
    
    .btn-login{
        background-color:#145DE8;
        color:white;   
    }
    
    .btn-registrarse{
        background-color:#BD0FAD;
        color:white;   
    }
    .btn-link{
        text-decoration: none;
        color:white;   

    }
    
    p{
        text-align:center;
        color:red;
        
    }
</style>