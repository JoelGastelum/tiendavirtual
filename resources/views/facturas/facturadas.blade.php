<div class="row">
    <div class="card">
        <div class="content">
                @csrf
                <h2 class="title"> Ver Facturas</h2>
                <table class="table">
                    <thead>
                        <tr>
                            <th>
                                Nombre
                            </th>
                            <th>
                                Precio
                            </th>
                            <th>
                                Impueso
                            </th>
                            <th>
                                Comprar
                            </th>
                        </tr>
                    </thead>  
                    <tbody>
                    @foreach($facturas as $factura)
                    <tr>
                        <td>
                            {{$factura -> User -> name}}
                        </td> 
                        <td>
                            {{$factura -> totalFactura}}
                        </td> 
                        <td>
                            {{$factura -> totalImpuesto}}
                        </td> 
                        <td>
                           <a href="/facturas/detalle/{{$factura->id}}"> 
                               <button class="btn">
                                    Detalle Factura
                                </button>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>  
                </table>
                @if(isset($success))
                    <p class="success"> {{$success}}</p>
                @endif
                
                @if(isset($error))
                    <p class="error"> {{$error}}</p>
                @endif
                <div class="form-input">
                    <a href="/dashboard" class="btn-link"><button class="btn btn-registrarse" type="button"> Regresar</button></a>
                </div>
          
            
        
        </div>
    </div>

</div>


<style>
 
    .row{
        display: flex;
        justify-content:center;
        align-items:center;
    }
    .card{
        margin-top:8rem;
        border:   0.3px #444444;
        box-shadow: 0 0 10px #444444;
        min-width:30rem;
    }
    
    .title {
        font-size:3em;
        text-align:center;
    }
    
    .content{
        display:flex;
        padding:1em;
        flex-direction:column;
        
    }
    
    .form-control{
        display:flex;
        border-radius:1em;
        width:100%;
        height:3em;
        margin:.4em;
    }
    
    .form-input{
        margin:2rem;
        align-self:center;
        text-align:center;
    }
    
    .btn {
        width:100%;
        border-radius:8px;
        margin:0.3rem;
        height:2rem;
    }
    
    .btn-login{
        background-color:#145DE8;
        color:white;   
    }
    
    .btn-registrarse{
        background-color:#BD0FAD;
        color:white;   
    }
    .btn-link{
        text-decoration: none;
        color:white;   

    }
    
    p{
        text-align:center;
        color:red;
        
    }
    
    .success{
        color:green;
    }
    
    .error{
        color:red;
    }
    .table {
        width : 100%;
        text-align:center;
    }
</style>