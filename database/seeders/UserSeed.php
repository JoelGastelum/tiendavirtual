<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        User::create([
            'id' => 1,
            'name' => 'admin',
            'email' => 'admin123@gmail.com',
            'password' => '123',
            'rol_id' => 2

        ]);
        
        User::create([
            'id' => 2,
            'name' => 'cliente',
            'email' => 'cliente123@gmail.com',
            'password' => '123',
            'rol_id' => 1

        ]);
    }
}
