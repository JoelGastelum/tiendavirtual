<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Rol;

class RolSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Rol::truncate();
        
        Rol::create([
            'id' => 1,
            'rol' => 'Cliente',
        ]);
        
        Rol::create([
            'id' => 2,
            'rol' => 'Administrador',
        ]);
    }
}
