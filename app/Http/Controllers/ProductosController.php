<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Producto;
use App\Models\Compra;


class ProductosController extends Controller
{
    //
    public function index(){
        $productos = Producto::get();
        return view('productos.comprar',['productos'=>$productos]);
    }
    
    public function guardar(Request $request){
        $data =Validator::make($request->all(),[
            'precio' => 'required|numeric',
            'impuesto' => 'required|numeric'
        ]);
        
        if($data -> fails()){
            return view('productos.create',['error' => 'Ocurrio un error al guardar']);
       }
       
       if(Producto::create($request->all())){
           
            return redirect()->route('dashboardView');

           
       }else{
           
            return view('productos.create',['error' => 'Ocurrio un error al guardar']);
           
       }
        
        
        
    }
    
    
    public function comprar($producto){
      
        $productos = Producto::get();
        
        if($compra= Compra::create([
            'producto_id'=> $producto,
            'user_id' => auth()->user()->id
        ])){
            
            return view('productos.comprar',['productos'=>$productos,'success' => 'Se guardo la compra correctamente']);
            
        }else{
            return view('productos.comprar',['productos'=>$productos , 'error' => 'Se encontro un error al guardar']);
            
        }
    }
}
