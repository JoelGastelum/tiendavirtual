<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Factura;
use App\Models\Compra;
use DB;


class FacturaController extends Controller
{
    //
    public function index(){
        $facturas = DB::select(
        'select sum(products.impuesto) as impuesto,sum(products.precio) as precio,users.name as nombre,users.id as id_user
        from compras 
        inner join products on products.id = compras.producto_id
        inner join users on users.id = compras.user_id
        where compras.factura_id = 0 GROUP by compras.user_id,users.name,users.id ');
        
        return view('facturas.pendientes',['facturas' => $facturas]);
        
    }
    
    public function facturar($iduser){
        $facturas = DB::select(
            "select sum(products.impuesto) as impuesto,sum(products.precio) as precio,users.name as nombre,users.id as id_user
            from compras 
            inner join products on products.id = compras.producto_id
            inner join users on users.id = compras.user_id
            where compras.factura_id = 0  and users.id='$iduser' GROUP by compras.user_id,users.name,users.id ");
            
            $factura = Factura::create(
                [
                    'totalImpuesto' => $facturas[0]->impuesto,
                    'totalFactura' => $facturas[0]->precio,
                    'cliente' => $facturas[0]->id_user,
                    
                ]
                );
            
            $compras = DB::select(
                "select  compras.id
                from compras 
                inner join products on products.id = compras.producto_id
                inner join users on users.id = compras.user_id
                where compras.factura_id = 0 and users.id='$iduser'  ");
            foreach($compras as $compra){
                $compra= Compra::findOrFail($compra->id);
                $compra -> factura_id = $factura->id;
                $compra->save();
                
            }
            
            
            return redirect()->route('facturas.pendientes');

            
            
    }
    
    public function facturadas (){
        $facturas = Factura::with('User')->get();
        
        
        return view('facturas.facturadas',['facturas'=> $facturas]);
        
    }
    
    public function detalleFactura($idfactura){
        $facturas = Compra::where('factura_id','=',$idfactura)->with('User','Producto') -> get();
        
        return view('facturas.detalle',['facturas'=> $facturas]);

        
        
    }
}
