<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Auth;

class LoginController extends Controller
{
    //
    
    public function login(Request $request){
        $user = User::where('email' , $request->email) -> where('password',$request->password)->first();
        
        if($user){
            Auth::loginUsingId($user->id);
            
            return redirect()->route('dashboardView');

        }else{
            
            return view('sessionViews/login',['error'=>'Las credenciales no coinciden con ningun usuario']);
        }
        
        
    }
}
