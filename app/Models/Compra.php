<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Compra extends Model
{
    use HasFactory;
    protected $table = "compras";
    
    public $fillable = [
        'producto_id',
        'user_id',
        'factura_id'
    ];
    
    public function User(){
        return $this->BelongsTo('App\Models\User','user_id','id') -> withDefault();
        
    }
    
    
    public function Producto(){
        return $this->BelongsTo('App\Models\Producto','producto_id','id') -> withDefault();
        
    }
    
}
