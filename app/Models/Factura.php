<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Factura extends Model
{
    use HasFactory;
    use HasFactory;
    protected $table = "facturas";
    
    public $fillable = [
        'totalImpuesto',
        'totalFactura',
        'cliente'
    ];
    public function User(){
        return $this->BelongsTo('App\Models\User','cliente','id') -> withDefault();
        
    }
    
    
}
